# set base image (host OS)
FROM python:3.10.6-alpine

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set the working directory in the container
WORKDIR /app

# copy the content of the local src directory to the working directory
COPY . /app/

# install dependencies
RUN pip install -r requirements.txt
RUN touch /app/oc-lettings-site.sqlite3

EXPOSE 8000
CMD ["gunicorn", "--bind", ":8000", "--workers", "3", "site_config.wsgi:application"]